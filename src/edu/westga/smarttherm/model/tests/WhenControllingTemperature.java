package edu.westga.smarttherm.model.tests;

import static org.easymock.EasyMock.expect;

import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.westga.smarttherm.model.IHvacSystem;
import edu.westga.smarttherm.model.IThermometer;
import edu.westga.smarttherm.model.SmartThermostat;

/**
 * Test that the HVAC system behaves properly.
 * 
 * @author Kathryn Browning
 * @version October 17, 2015
 *
 */
@RunWith(EasyMockRunner.class)
public class WhenControllingTemperature extends EasyMockSupport {

	@TestSubject
	private SmartThermostat st = new SmartThermostat();

	@Mock
	private IHvacSystem hvacMock;
	@Mock 
	private IThermometer thermometerMock; 

	/**
	 * The set-up
	 */
	@Before()
	public void setUp() {
		this.st.setDesiredTemperature(72);
	}

	/**
	 * Test that a null HVAC should throw the proper exception.
	 */
	@Test(expected = IllegalStateException.class)
	public void nullHvacShouldThrowException() {
		//set up
		expect(this.hvacMock).andReturn(null);
		
		//execution
		this.st.measureTempAndOperateHVAC();
	}

	/**
	 * Test that a null temperature should throw the proper exception.
	 */
	@Test(expected = IllegalStateException.class)
	public void nullThermometerShouldThrowException() {
		//set up
		expect(this.thermometerMock).andReturn(null);
		
		//execution
		this.st.measureTempAndOperateHVAC();
	}

	/**
	 * Test that the HVAC turns on the A/C if the current temperature is greater
	 * than the desired temperature.
	 */
	@Test
	public void hvacShouldTurnAirOnIfCurrentTempGreaterThanDesiredTemp() {
		// set up
		expect(this.thermometerMock.getTemperature()).andReturn(78);

		this.hvacMock.cool();
		EasyMock.expectLastCall().once();
		replayAll();

		// execution
		this.st.measureTempAndOperateHVAC();

		// verification:
		// verify that the mock HVAC was called as defined in our set up,
		// which means that the test passes if the mock HVAC ends up having to
		// use the cool() method.
		EasyMock.verify(this.hvacMock);
	}

	/**
	 * Test that the HVAC turns on the head if the current temperature is less
	 * than the desired temperature.
	 */
	@Test
	public void hvacShouldTurnHeatOnIfCurrentTempLessThanDesiredTemp() {
		// set up
		expect(this.thermometerMock.getTemperature()).andReturn(65);

		this.hvacMock.heat();
		EasyMock.expectLastCall().once();
		replayAll();

		// execution
		this.st.measureTempAndOperateHVAC();

		// verification:
		// verify that the mock HVAC was called as defined in our set up, which
		// means that the test passes if the mock HVAC ends up having to use the
		// heat() method.
		EasyMock.verify(this.hvacMock);
	}

	/**
	 * Test that the HVAC neither heats nor cools if the current temperature is
	 * at the desired temperature. 
	 */
	@Test
	public void hvacShouldTurnOffIfCurrentTempIsAtDesiredTemp() {
		// setup
		expect(this.thermometerMock.getTemperature()).andReturn(72);

		this.hvacMock.turnOff();
		EasyMock.expectLastCall().once();
		replayAll();

		// execution
		this.st.measureTempAndOperateHVAC(); 

		// verification:
		// verify that the mock HVAC was called as defined in our set up, which
		// means that the test passes if the mock HVAC ends up having to use the
		// turnOff() method.
		EasyMock.verify(this.hvacMock);
	}
}
