package edu.westga.smarttherm.model.tests;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.*;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.easymock.EasyMockRule;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.westga.smarttherm.model.SetPoint;
import edu.westga.smarttherm.model.SmartThermostat;

@RunWith(EasyMockRunner.class)
public class WhenSchedulingDesiredTemperature {

	
	@Rule
	public EasyMockRule mocks = new EasyMockRule(this);
	
	@TestSubject
	private SmartThermostat st = new SmartThermostat();
	
	@Mock
	private Clock mock;
	
	
	private ZonedDateTime midnight;
	private ZonedDateTime noon;
	private ZonedDateTime oneMinuteToMidnight;
	private ZonedDateTime oneMinuteAfterMidnight;
	private ZonedDateTime zdt0730on07182000;
	private ZonedDateTime zdt2200on07182000;
	private ZonedDateTime zdt1415on07181998;
	
	private ZoneId timeZone;
	
	@Before
	public void setUp() {
		this.timeZone = ZoneId.of("UTC-5");
		this.midnight = ZonedDateTime.of(2000, 1, 1, 0, 0, 0, 0, this.timeZone);
		this.noon = ZonedDateTime.of(2000, 1, 1, 12, 0, 0, 0, this.timeZone);
		this.oneMinuteToMidnight = ZonedDateTime.of(2000, 1, 1, 23, 59, 0, 0, this.timeZone);
		this.oneMinuteAfterMidnight = ZonedDateTime.of(2000, 1, 1, 0, 1, 0, 0, this.timeZone);
		this.zdt0730on07182000 = ZonedDateTime.of(2000, 7, 18, 7, 30, 0, 0, this.timeZone);
		this.zdt2200on07182000 = ZonedDateTime.of(2000, 7, 18, 22, 0, 0, 0, this.timeZone);
		this.zdt1415on07181998 = ZonedDateTime.of(1998, 7, 18, 14, 15, 0, 0, this.timeZone);
	}
	
	@Test
	public void shouldSetAtMidnight() {
		expect(mock.instant())
			.andReturn(Instant.from(this.midnight));
		expect(mock.getZone())
			.andReturn(this.timeZone);
		
		replay(mock);
		this.st.setDesiredTemperature(72);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.MIDNIGHT, sched[0].getScheduledTime());
	}
	
	@Test
	public void shouldSetAtOnePastMidnight() {
		expect(mock.instant())
			.andReturn(Instant.from(this.oneMinuteAfterMidnight));
		expect(mock.getZone())
			.andReturn(this.timeZone);
		
		replay(mock);
		this.st.setDesiredTemperature(72);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.MIDNIGHT.plusMinutes(1), sched[0].getScheduledTime());
	}
	
	@Test
	public void shouldSetAtNoon() {
		expect(mock.instant())
			.andReturn(Instant.from(this.noon));
		expect(mock.getZone())
			.andReturn(this.timeZone);
		
		replay(mock);
		this.st.setDesiredTemperature(72);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.NOON, sched[0].getScheduledTime());
	}
	
	@Test
	public void shouldSetAtOneBeforeMidnight() {
		expect(mock.instant())
			.andReturn(Instant.from(this.oneMinuteToMidnight));
		expect(mock.getZone())
			.andReturn(this.timeZone);
		
		replay(mock);
		this.st.setDesiredTemperature(72);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.MIDNIGHT.minusMinutes(1), sched[0].getScheduledTime());
	}

	@Test
	public void shouldOrderTwoSetPointsAddedInOrder() {
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt0730on07182000));
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt2200on07182000));
		expect(mock.getZone())
			.andReturn(this.timeZone).times(2);
		replay(mock);
		this.st.setDesiredTemperature(75);
		this.st.setDesiredTemperature(65);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.from(this.zdt0730on07182000), LocalTime.from(sched[0].getScheduledTime()));
		assertEquals(LocalTime.from(this.zdt2200on07182000), LocalTime.from(sched[1].getScheduledTime()));
	}
	
	@Test
	public void shouldOrderTwoSetPointsAddedOutOfOrder() {
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt2200on07182000));
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt0730on07182000));
		expect(mock.getZone())
			.andReturn(this.timeZone).times(2);
		replay(mock);
		this.st.setDesiredTemperature(75);
		this.st.setDesiredTemperature(65);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.from(this.zdt0730on07182000), LocalTime.from(sched[0].getScheduledTime()));
		assertEquals(LocalTime.from(this.zdt2200on07182000), LocalTime.from(sched[1].getScheduledTime()));
	}
	
	@Test
	public void shouldIgnoreDateWhenScheduling() {
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt1415on07181998));
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt0730on07182000));
		expect(mock.getZone())
			.andReturn(this.timeZone).times(2);
		replay(mock);
		this.st.setDesiredTemperature(75);
		this.st.setDesiredTemperature(65);
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.from(this.zdt0730on07182000), LocalTime.from(sched[0].getScheduledTime()));
		assertEquals(LocalTime.from(this.zdt1415on07181998), LocalTime.from(sched[1].getScheduledTime()));
	}
	
	@Test
	public void canScheduleManySetPoints() {
		expect(mock.instant())
			.andReturn(Instant.from(this.midnight));
		expect(mock.instant())
			.andReturn(Instant.from(this.noon));
		expect(mock.instant())
			.andReturn(Instant.from(this.oneMinuteToMidnight));
		expect(mock.instant())
			.andReturn(Instant.from(this.oneMinuteAfterMidnight));
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt0730on07182000));
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt2200on07182000));
		expect(mock.instant())
			.andReturn(Instant.from(this.zdt1415on07181998));
		expect(mock.getZone())
			.andReturn(this.timeZone).times(7);
		replay(mock);
		for (int i = 0; i < 7; i++) {
			this.st.setDesiredTemperature(70);
		}
		SetPoint[] sched = this.st.getSchedule();
		verify(mock);
		assertEquals(LocalTime.from(this.midnight), LocalTime.from(sched[0].getScheduledTime()));
		assertEquals(LocalTime.from(this.oneMinuteAfterMidnight), LocalTime.from(sched[1].getScheduledTime()));	
		assertEquals(LocalTime.from(this.zdt0730on07182000), LocalTime.from(sched[2].getScheduledTime()));
		assertEquals(LocalTime.from(this.noon), LocalTime.from(sched[3].getScheduledTime()));
		assertEquals(LocalTime.from(this.zdt1415on07181998), LocalTime.from(sched[4].getScheduledTime()));
		assertEquals(LocalTime.from(this.zdt2200on07182000), LocalTime.from(sched[5].getScheduledTime()));
		assertEquals(LocalTime.from(this.oneMinuteToMidnight), LocalTime.from(sched[6].getScheduledTime()));
		
	}
}
