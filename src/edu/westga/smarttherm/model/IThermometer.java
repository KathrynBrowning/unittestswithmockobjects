package edu.westga.smarttherm.model;

/**
 * A Thermometer is something that can measure temperature.
 * 
 * @author lewisb
 *
 */
public interface IThermometer {
	
	/**
	 * Gets the temperature
	 * 
	 * @return the measured temperature
	 */
	int getTemperature();
}
